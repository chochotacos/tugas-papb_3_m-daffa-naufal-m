package com.example.potato_head;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView hairImageView;
    private ImageView beardImageView;
    private ImageView moustacheImageView;
    private ImageView eyebrowImageView;

    private CheckBox hairCheckBox;
    private CheckBox beardCheckBox;
    private CheckBox moustacheCheckBox;
    private CheckBox eyebrowCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Menghubungkan ImageView dengan ID-nya
        hairImageView = findViewById(R.id.imageView8);
        beardImageView = findViewById(R.id.imageView12);
        moustacheImageView = findViewById(R.id.imageView15);
        eyebrowImageView = findViewById(R.id.imageView10);

        // Menghubungkan CheckBox dengan ID-nya
        hairCheckBox = findViewById(R.id.checkBox);
        beardCheckBox = findViewById(R.id.checkBox4);
        moustacheCheckBox = findViewById(R.id.checkBox5);
        eyebrowCheckBox = findViewById(R.id.checkBox2);

        // Mengatur listener untuk setiap CheckBox
        hairCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hairImageView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        beardCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                beardImageView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        moustacheCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                moustacheImageView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        eyebrowCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                eyebrowImageView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
    }
}
